# Functional programming ++ (Part 2: Theory)

## Monads, WTF?

Let's get to know some examples before we look at theory.

### Maybe

```javascript
var Maybe = function(x) {
  this.__value = x
}

Maybe.of = function(x) {
  return new Maybe(x)
}

Maybe.prototype.isNothing = function() {
  return (this.__value === null || this.__value === undefined)
}

Maybe.prototype.map = function(f) {
  return this.isNothing() ? Maybe.of(null) : Maybe.of(f(this.__value))
}
```
Usage:
```javascript
Maybe.of('Malkovich Malkovich').map(match(/a/ig))
//=> Maybe(['a', 'a'])

Maybe.of(null).map(match(/a/ig))
//=> Maybe(null)
```
Use cases:

* When a function might fail to return a result
* optional fields

### IO

```javascript
var IO = function(f) {
  this.__value = f
}

IO.of = function(x) {
  return new IO(function() {
    return x
  })
}

IO.prototype.map = function(f) {
  return new IO(_.compose(f, this.__value))
}
```
Usage:
```javascript
//  io_window :: IO Window
var io_window = new IO(function() {
  return window
})

io_window.map(function(win) {
  return win.innerWidth
})
// IO(1430)

io_window.map(_.prop('location')).map(_.prop('href')).map(_.split('/'))
// IO(["http:", "", "localhost:8000", "blog", "posts"])


//  $ :: String -> IO [DOM]
var $ = function(selector) {
  return new IO(function() {
    return document.querySelectorAll(selector)
  })
}

$('#myDiv').map(head).map(function(div) {
  return div.innerHTML
})
// IO('I am some inner html')
```

Use case: Side effects

### List

```javascript
// we don't need this, it's build-in
var List = function(...args) {
  this.__value = [...args]
}

List.of = function(...args) {
  return new List(...args)
}

List.prototype.map = function(f) {
  return this.__value.map(f)
}
```

Usage:
```
var arr = [1, 2, 3]

arr.map(x => x + 1) // [2, 3, 4]
```

### Summary

* Maybe: Container with something or nothing inside
* IO: Container with a function (that usually causes a side-effect) inside
* List: Container with an arbitrary number of values inside

## Category theory

A category is a collection with the following components:

* A collection of objects (e.g. data types)
* A collection of morphisms (e.g. functions)
* A notion of composition on the morphisms (e.g. `compose`)
* A distinguished morphism called identity (e.g. identity function)

Example:
```javascript
var g = function(x) {
  return x.length
}
var f = function(x) {
  return x === 4
}
var isFourLetterWord = compose(f, g)
```

![compose](https://drboolean.gitbooks.io/mostly-adequate-guide/content/images/cat_comp1.png)
![compose2](https://drboolean.gitbooks.io/mostly-adequate-guide/content/images/cat_comp2.png)

Identity function (`id = x => x`) can serve as placeholder for data. It has to satisfy the following requirement for any one-argument function `f`:
```javascript
compose(id, f) == compose(f, id) == f
```

[Wikipedia](https://en.wikipedia.org/wiki/Category_theory)

## type signatures (Hindley-Milner)

* Simple case:
  `capitalize :: String -> String`
* Multiple arguments reflect currying:
  `join :: String -> [String] -> String`
* Generic types:
  `map :: (a -> b) -> [a] -> [b]`
* Constraints:
  `sort :: Ord a => [a] -> [a]`
  `assertEqual :: (Eq a, Show a) => a -> a -> Assertion`

## Some typeclasses

A typeclass is like an interface. It defines some behavior. If a type is a part of a typeclass it supports (implements) the behavior the typeclass describes.

![tree](https://github.com/fantasyland/fantasy-land/raw/master/figures/dependencies.png)

### Functors

Analogy: Container, Mappable, Group of data types

= Something we can `map` over

[In pictures](http://adit.io/posts/2013-04-17-functors,_applicatives,_and_monads_in_pictures.html#functors)

[Specification in fantasy-land](https://github.com/fantasyland/fantasy-land#functor)

Math:
> Let C and D be categories. A functor F from C to D is a mapping that
>
> 1. associates to each object `X` in C an object `F(X)` in D,
> 2. associates to each morphism `f: X -> Y` in C a morphism `F(f): F(X) -> F(Y)` in D such that the following two conditions hold:
>   * `F(id) = id` for every object `X` in C,
>   * `F(g * f) = F(g) * F(f)` for all morphisms `f: X -> Y` and `g: Y -> Z` in C.
> That is, functors must preserve identity morphisms and composition of morphisms.

### Pointed functor

A functor with an `of` method: `of :: Pointed f => a -> f a`. Example for lists:
```javascript
const of = x => [x]
```

### Applicatives

= Pointed functors that can be applied to other functors

Example: Apply a list of functions to a list of values.
```javascript
R.ap([R.multiply(2), R.add(3)], [1,2,3]) //=> [2, 4, 6, 4, 5, 6]
```
[R.ap docs](http://ramdajs.com/docs/#ap)

[In pictures](http://adit.io/posts/2013-04-17-functors,_applicatives,_and_monads_in_pictures.html#applicatives)


`ap` is a function that can apply the function contents of one functor to the value contents of another.

Example:
```javascript
// we can't do this because the numbers are bottled up.
add(Container.of(2), Container.of(3))
//NaN

Container.of(add(2)).ap(Container.of(3))
```

Implementation of `ap`:
```javascript
Container.prototype.ap = function(other_container) {
  return other_container.map(this.__value)
}
```
[Specification in fantasy-land](https://github.com/fantasyland/fantasy-land#applicative)

### Chain

Motivation: Containers nested inside containers

```javascript
//  safeProp :: Key -> {Key: a} -> Maybe a
var safeProp = curry(function(x, obj) {
  return new Maybe(obj[x])
})

//  safeHead :: [a] -> Maybe a
var safeHead = safeProp(0)

//  firstAddressStreet :: User -> Maybe (Maybe (Maybe Street) )
var firstAddressStreet = compose(
  map(map(safeProp('street'))), map(safeHead), safeProp('addresses')
)

firstAddressStreet({
  addresses: [{
    street: {
      name: 'Mulburry',
      number: 8402,
    },
    postcode: 'WC2N',
  }],
})
// Maybe(Maybe(Maybe({name: 'Mulburry', number: 8402})))
```

Define `join`:
```javascript
var mmo = Maybe.of(Maybe.of('nunchucks'))
// Maybe(Maybe('nunchucks'))

mmo.join()
// Maybe('nunchucks')
```

```javascript
//  firstAddressStreet :: User -> Maybe Street
var firstAddressStreet = compose(
  join, map(safeProp('street')), join, map(safeHead), safeProp('addresses')
)

firstAddressStreet({
  addresses: [{
    street: {
      name: 'Mulburry',
      number: 8402,
    },
    postcode: 'WC2N',
  }],
})
// Maybe({name: 'Mulburry', number: 8402})
```
Often we're calling `join` right after `map`. Let's define `chain` (aka `flatMap`):
```javascript
// chain :: Chain m => (a -> m b) -> m a -> m b
var chain = curry(function(f, m){
  return compose(join, map(f))(m)
})
```
```javascript
var firstAddressStreet = compose(
  chain(safeProp('street')), chain(safeHead), safeProp('addresses')
)
```

Example with lists:
```javascript
var duplicate = n => [n, n];
R.chain(duplicate, [1, 2, 3]); //=> [1, 1, 2, 2, 3, 3]

R.chain(R.append, R.head)([1, 2, 3]); //=> [1, 2, 3, 1]
```

[Specification in fantasy-land](https://github.com/fantasyland/fantasy-land#monad)

### Monads

= Applicative + Chain

`ap` and `chain` can be expressed as one function `bind:: m a -> (a -> m b) -> m b`.

[In pictures](http://adit.io/posts/2013-04-17-functors,_applicatives,_and_monads_in_pictures.html#monads)

[Specification in fantasy-land](https://github.com/fantasyland/fantasy-land#monad)


