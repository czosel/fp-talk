/**
 * imperative patterns
 *
 * var for state in functions
 * assignment for updates: x = x + 1
 * property access: obj.prop
 * for, while
 * if, then, else, ? :
 * +, -, *, /
 */

// loops
const logAll = arr => {
  for (let entry of arr) {
    console.log(entry)
  }
}

const logAll2 = arr.forEach(entry => {
  console.log(entry)
})
// -> use map, filter, reduce, every, some, find

// composition
const doTwoThings = a => {
  var b = doFirst(a)
  return doLast(b)
}

const doTwoThings2 = compose(doLast, doFirst)
const doTwoThings3 = pipe(doFirst, doLast)

// pointfree
const square = x => x * x

const squareAll = arr => {
  return arr.map(sqare)
}
const square2 = map(square)
