const test = require('tape')
const fs = require('fs')
const { IO, Maybe, Future } = require('ramda-fantasy')

const { Just, Nothing } = Maybe

/**
 * The IO type is used to store a function that describes some computation with side effects.
 *
 * constructor: Constructs an IO instance, which represents some action, possibly with side effects
 * :: (() => a) -> IO a
 *
 * runIO: executes the action
 * :: IO a -> a
 *
 */

//:: String -> IO String
const readFile = fileName => IO(() => fs.readFileSync(fileName, 'utf8'))
test('IO', t => {
  t.ok(readFile('package.json').runIO().startsWith('{'))
  t.end()
})

/**
 * The Maybe type represents the possibility of some value or nothing.
 */

//:: Number -> Maybe Number
const safeDiv = (n, d) => (d === 0 ? Nothing() : Just(n / d))

test('Maybe', t => {
  t.deepEqual(safeDiv(10, 2), Just(5))
  t.deepEqual(safeDiv(10, 0), Nothing())
  t.end()
})

/**
 * The Future type is used to represent some future, often asynchronous, action that may potentially fail.
 */

//:: String -> Future String
const readFileSafe = fileName =>
  Future((reject, resolve) => {
    fs.readFile(fileName, 'utf8', (err, data) => {
      if (err) {
        reject(err)
      }
      resolve(data)
    })
  })

test('Future', t => {
  t.plan(1)
  readFileSafe('package.json').fork(
    err => console.log('error', err),
    data => t.ok(data)
  )
})
