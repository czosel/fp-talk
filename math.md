# Algebraic data types

## Functor

Let C and D be categories. A functor F from C to D is a mapping that

1. associates to each object `X` in C an object `F(X)` in D,
2. associates to each morphism `f: X -> Y` in C a morphism `F(f): F(X) -> F(Y)` in D such that the following two conditions hold:
  * `F(id) = id` for every object `X` in C,
  * `F(g * f) = F(g) * F(f)` for all morphisms `f: X -> Y` and `g: Y -> Z` in C.
That is, functors must preserve identity morphisms and composition of morphisms.

The above definition defines a _covariant_ functor. A _contravariant functor_ reverses the direction of composition: `F(g * f) = F(f) * F(g)`

Endofunctor: A functor that maps a category to itself.

Examples:

Lists are an endofunctor:
  1. `map :: [] -> []` (associates each list to another list)
  2. Example, list of integers
    * id = n => n; map(id) = id_[] (map with the identity function for integers is equal to the identity function in the category of lists of integers)
    * `half: n -> n / 2`, `add1: n -> n + 1`, map(half * add1) = map(half) * map(add1), i.e. "(add one and divide by 2) every entry in [1,2,3]" is equal to "(add one to every entry in [1,2,3]) and then (divide every entry in [1,2,3] by 2)
`map :: (n -> n) -> [] -> []`


Maybe is a functor
