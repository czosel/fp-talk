const test = require('tape')
const R = require('ramda')

const isEven = x => x % 2 === 0

test('curry', t => {
  t.deepEqual(R.filter(isEven, [1, 2, 3]), [2])

  const filterEven = R.filter(isEven)
  t.deepEqual(filterEven([1, 2, 3]), [2])

  t.deepEqual(R.filter(isEven)([1, 2, 3]), [2])
  t.end()
})

test('combine', t => {
  const isOdd = R.complement(isEven)
  t.ok(isOdd(3))
  t.notok(isOdd(2))
  t.end()
})
