## Reading material

Getting started
* [Functors, Applicatives, And Monads In Pictures](http://adit.io/posts/2013-04-17-functors,_applicatives,_and_monads_in_pictures.html)

Combining functions, partial application, currying, pointfree, lenses
* [Thinking in ramda](http://randycoulman.com/blog/categories/thinking-in-ramda/)

Algebraic data types
* [Haskell Typeclassopedia](https://wiki.haskell.org/Typeclassopedia)

JS Libraries

* [Ramda](http://ramdajs.com/)
* [ramda-fantasy](https://github.com/ramda/ramda-fantasy)
* [Sanctuary](https://sanctuary.js.org/)
* [Folktale](http://folktalejs.org/)
