const { IO, Maybe, Future } = require('ramda-fantasy')
const R = require('ramda')

// debugging
const trace = R.curry((tag, x) => {
  console.log(tag, x)
  return x
})

/*const demo = {
  async _getCustomer() {
    // @TODO Error Handling when backend gives an error
    const locationId = this.modelFor('choose').get('location.id')
    try {
      const customer = await this._getCustomerFromApi('customer', locationId)
      if (customer) {
        return customer
      }
      const sfCustomer = await this._getCustomerFromApi(
        'salesforce',
        locationId
      )
      const newCustomer = this.store.createRecord('customer')
      if (sfCustomer) {
        return this._copyCustomerData(sfCustomer, newCustomer)
      }
      return newCustomer
    } catch (e) {
      console.error(e)
    }
  },
  async _getCustomerFromApi(endpoint, locationId) {
    const customerList = await this.store.query(endpoint, {
      include: 'country,language,title',
      filter: {
        location: locationId
      }
    })
    if (customerList.length) {
      return customerList.get('firstObject')
    }
    return null
  }
}*/

// getCustomerFromApi :: endpoint -> locationId -> Future[Customer]
const getCustomerFromApi = R.curry((endpoint, locationId) =>
  Future.of({
    name: 'Max Muster'
  })
)

// transform :: SfCustomer -> Customer
// TODO dummy
const transform = sfCustomer => sfCustomer

// getLocationId :: context -> locationId
const getLocationId = ctx => ctx.modelFor('choose').get('location.id')

// emptyCustomer :: context -> Customer
const emptyCustomer = ctx => ctx.store.createRecord('customer')

// getSfCustomer :: locationId -> Future[SfCustomer]
const getSfCustomer = getCustomerFromApi('salesforce')

// fromSfCustomer :: locationId -> Future[Customer]
const fromSfCustomer = R.pipe(getSfCustomer, transform)

// fromExistingCustomer :: locationId -> Future[Customer]
const fromExistingCustomer = getCustomerFromApi('customer')

// getCustomerFromLocation :: locationId -> Future[Customer]
const getCustomerFromLocation = R.defaultTo(
  R.defaultTo(emptyCustomer, fromSfCustomer),
  fromExistingCustomer
)

// getCustomer :: context -> Future[Customer]
const getCustomer = R.pipe(getLocationId, getCustomerFromLocation)

module.exports = {
  getCustomerFromApi,
  getCustomer
}
