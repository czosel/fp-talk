const test = require('tape')
const tcs = require('./ember')

const ctx = {
  store: {},
  modelFor(name) {
    return {
      get() {
        return 5
      }
    }
  }
}

test('getCustomerFromApi', t => {
  t.plan(1)
  tcs
    .getCustomerFromApi('customer', 5)
    .fork(
      err => console.error('err', err),
      data => t.equal(data.name, 'Max Muster')
    )
})

test('getCustomer', t => {
  t.plan(1)
  tcs
    .getCustomer(ctx)
    .fork(
      err => console.error('err', err),
      data => t.equal(data.name, 'Max Muster')
    )
})
