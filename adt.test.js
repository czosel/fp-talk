const test = require('tape')
const { map, compose } = require('ramda')

/**
 * Functor
 * =======
 *
 * Simple definition: a type that can be mapped over
 * Metaphores: Container (List, Tree), computational context (Maybe)
 * map :: (a -> b)   -> fa        -> fb
 *        (function)    (functor) -> (functor)
 *
 * map is called "fmap" in haskell
 */

// addOne :: Int -> Int
const addOne = x => x + 1

test('"map" on arrays is a functor', t => {
  t.deepEqual(map(addOne, [1, 2, 3]), [2, 3, 4])
  t.end()
})

// comment :: Int -> String
const comment = x => x + ', what a nice number!'

test('functions can be functors too, if "map" is defined', t => {
  Function.prototype.map = function(f) {
    // we can define a map on functions that runs the passed function after the function itself
    return compose(f, this)
  }

  commentOnMore = addOne.map(comment)
  t.equal(commentOnMore(4), '5, what a nice number!')

  t.end()
})

// cmap (a -> b) ->
test('functions can be contravariant functors', t => {
  Function.prototype.cmap = function(f) {
    // we can define a map on functions that runs the passed function after the function itself
    return compose(this, f)
  }

  commentOnHalf = comment.cmap(x => x / 2) // read: "please apply this function before"
  t.equal(commentOnHalf(4), '2, what a nice number!')

  t.end()
})

test('combining covariant and contravariant functors', t => {
  const shoutCommentOnHalf = comment.cmap(x => x / 2).map(x => x.toUpperCase())
  t.equal(shoutCommentOnHalf(2), '1, WHAT A NICE NUMBER!')
  t.end()
})
