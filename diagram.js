const $ = require('jquery')
const { IO, Maybe, Future } = require('ramda-fantasy')
const { addIndex, map, compose, join, multiply } = require('ramda')

function Point(x, y) {
  if (!(this instanceof Point)) {
    return new Point(x, y)
  }
  this.x = x
  this.y = y
}
Point.prototype.toString = function() {
  return `Point [${this.x}, ${this.y}]`
}
Point.prototype.inspect = function() {
  return this.toString()
}
Point.prototype.map = function(fn) {
  return Point(...[this.x, this.y].map(fn))
}
Point.prototype.join = function(separator) {
  return [this.x, this.y].join(separator)
}

//:: String -> IO String
const render = html => IO(() => $('#chart').html(html))

//asPoints:: Array Int -> Array Point
const listAsPoints = addIndex(map)((y, i) => Point(i, y))
//:: Array Point -> String
const formatPoints = compose(join(' '), map(join(',')))

//:: Array Point -> Array Point
const project = map(map(multiply(10)))

const asPoints = compose(formatPoints, project, listAsPoints)

const diagram = data => `
<svg height="200px" viewBox="0 0 300 200">
  <polyline
     fill="none"
     stroke="#FF5252"
     stroke-width="3"
     points="${asPoints(data)}"/>
</svg>`

const lineChart = compose(render, diagram)

module.exports = {
  listAsPoints,
  asPoints,
  diagram,
  lineChart,
  Point
}
