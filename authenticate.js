const test = require('tape')

const fetchStub = () =>
  new Promise((resolve, reject) =>
    resolve({
      ok: 'ok',
      json() {
        return new Promise(resolve =>
          resolve({
            token: '123'
          })
        )
      }
    })
  )

const authenticate = async ({ username, password }) => {
  const response = await fetchStub('/api/v1/edispo/auth/login', {
    body: JSON.stringify({ username, password })
  })

  const json = await response.json()

  if (!response.ok) {
    throw new Error(json)
  }

  if (!json.token) {
    throw new Error('JWT Token not found...')
  }

  return json
}

test('authenticate', async t => {
  const json = await authenticate({})
  t.equal(json.token, '123')
  t.end()
})
