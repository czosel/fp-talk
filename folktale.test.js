const test = require('tape')
const Maybe = require('folktale/data/maybe')
const { task } = require('folktale/data/task')
const fs = require('fs')

const half = n => (n % 2 === 0 ? Maybe.Just(n / 2) : Maybe.Nothing())

test('half', t => {
  t.deepEqual(half(3), Maybe.Nothing())
  t.deepEqual(half(2), Maybe.Just(1))
  t.end()
})

// read : String -> Task(Error, Buffer)
const read = path =>
  task(({ resolve, reject }) => {
    fs.readFile(path, (error, data) => {
      if (error) reject(error)
      else resolve(data)
    })
  })

test('read file with task', async t => {
  // read actual file
  const data = await read('package.json').run().promise()
  t.ok(data.toString('utf-8').startsWith('{'))
  t.end()
})

test('read nonexisting file', async t => {
  t.plan(1)
  try {
    await read('doesnt-exist.json').run().promise()
  } catch (e) {
    t.equal(e.code, 'ENOENT')
  }
})
