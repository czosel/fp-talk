/**
 * Functional programming ++
 *
 * Last time: general concepts, introduction
 * This time: repetition, concrete usage examples, a step further?
 *
 * Sources:
 * - https://drboolean.gitbooks.io/mostly-adequate-guide/
 */

/**
 * =====================
 * first class functions
 * =====================
 */
const hi = function(name) {
  return 'Hi ' + name
}

const greeting = function(name) {
  return hi(name)
}

const getServerStuff = function(callback) {
  return ajaxCall(function(json) {
    return callback(json)
  })
}

// naming things

// specific to our current blog
const validArticles = function(articles) {
  return articles.filter(function(article) {
    return article !== null && article !== undefined
  })
}

// vastly more relevant for future projects
const compact = function(xs) {
  return xs.filter(function(x) {
    return x !== null && x !== undefined
  })
}

/**
 * ==============
 * pure functions
 * ==============
 * same input => same output
 * no side effects
 */

// slice vs. splice
let xs = [1, 2, 3, 4, 5]
// pure
xs.slice(0, 3) //=> [1, 2, 3]
xs.slice(0, 3) //=> [1, 2, 3]
xs.slice(0, 3) //=> [1, 2, 3]

// impure
xs.splice(0, 3) //=> [1, 2, 3]
xs.splice(0, 3) //=> [4, 5]
xs.splice(0, 3) //=> []

// push
xs = [1, 2]
xs.push(3) //=> [1, 2, 3]
xs.push(4) //=> [1, 2, 3, 4]

// add element to array without mutating it?
const pushImmutable = (arr, el) => {
  return
}

/**
 * Why pure?
 *
 * Cacheable (memoization)
 * Portable / Self-documenting
 * Testable
 * Reasonable
 * Parallelizable
 */

/**
 * ========
 * Currying
 * ========
 */

const add = (x, y) => x + y
add(2) //=> NaN

const curriedAdd = x => y => x + y
const add2 = curriedAdd(2)
add2(3) //=> 5

curriedAdd(2, 3) // ?

const curriedAdd2 = R.curry(add)
curriedAdd2(2)(3) //=> 5
curriedAdd2(2, 3) //=> 5
/**
 * Ramda: http://ramdajs.com/
 *
 * https://github.com/ramda/ramda/blob/master/src/curry.js
 * https://github.com/ramda/ramda/blob/master/src/curryN.js
 */

// argument order
const square = x => x * x

const squareAll = arr => {
  return arr.map(square)
}

const map = R.curry((f, arr) => arr.map(f))

const squareAll2 = map(square)

/**
 * ===========
 * Composition
 * ===========
 */

let doTwoThings = a => {
  var b = doFirst(a)
  return doSecond(b)
}

doTwoThings = a => doSecond(doFirst(a))

const compose = (f, g) => x => f(g(x))

doTwoThings = compose(doSecond, doFirst)
doTwoThings = pipe(doFirst, doSecond)

/**
 * https://github.com/ramda/ramda/blob/master/src/compose.js
 * https://github.com/ramda/ramda/blob/master/src/pipe.js
 * https://github.com/ramda/ramda/blob/master/src/internal/_pipe.js
 */

// Pointfree style
const initials = function(name) {
  return name.split(' ').map(compose(toUpperCase, head)).join('. ')
}

const initials2 = R.compose(
  R.join('. '),
  R.map(R.compose(toUpperCase, R.head)),
  R.split(' ')
)

initials('hunter stockton thompson') //=> 'H. S. T'

// debugging
const trace = R.curry((tag, x) => {
  console.log(tag, x)
  return x
})

/**
 * Declarative vs. imperative
 * ==========================
 * - describe "_what_ you want as a result" instead of "_how_ to get a result"
 * - expressions instead of instructions
 * - declarative code has no sense of "evaluation order"
 *
 * Example for declarative style: SQL
 */

// imperative
let makes = []
for (let i = 0; i < cars.length; i++) {
  makes.push(cars[i].make)
}

// declarative
const makes2 = cars.map(car => car.make)

// imperative
const authenticate = form => {
  const user = toUser(form)
  return logIn(user)
}

// declarative
const authenticate2 = compose(logIn, toUser)
